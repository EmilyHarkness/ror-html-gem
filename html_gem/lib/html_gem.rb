require 'rack/utils'
# gem 'rack', git: 'https://github.com/rack/rack'

module HTMLGem
  def self.create_html_file(content, file_name = 'index', bypass_html = false)
    file = File.new("#{file_name}.html", "w")
    if bypass_html
      file.puts(content)
    else
      file.puts(Rack::Utils.escape_html(content))
    end

    file.close
  end
end
